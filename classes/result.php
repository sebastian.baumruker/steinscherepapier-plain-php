<?php

Class Result {
    private $resultMatrixArr = [
        "stein" => "schere",
        "schere" => "papier",
        "papier" => "stein"
    ];

    function __construct($input, $symbol) {
        $this->input = $input;
        $this->symbol = $symbol;
    }

    public function getResult() {
        if($this->input === $this->symbol) {
            return "Unentschieden";
        }
        else {
            if($this->symbol === $this->resultMatrixArr[$this->input]) {
                return "Glückwunsch, du hast gewonnen!";
            }
        }
        return "Leider verloren";
    }

    public function getSymbol() {
        return $this->symbol;
    }

    public function getInput() {
        return $this->input;
    }
}