<?php
include_once "classes/symbol.php";
include_once "classes/result.php";

if(isset($_POST['value'])) {
    $input = $_POST['value'];
    $symbol = new Symbol();
    $result = new Result($input, $symbol->randomSymbol());
}
else {
    $result = false;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stein, Schere, Papier</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <?php
        if(!$result) {
            echo "<h2 class='firstTitle'>Wähle Stein, Schere oder Papier</h2>";
        }
        else {
            echo "<h2 class='resultTitle'>".$result->getResult()."</h2>";
            echo "<div class='opponent ".$result->getSymbol()."'></div>";
            echo "<h4 class='playAgain'>Um noch einmal zu spielen klick Stein, Schere oder Papier.</h4>";
        }
    ?>
    <div class="wrapper">
        <form method="post">
            <input type="hidden" name="value" value="stein">
            <input type="submit" class="stein <?php echo($result) ? (($result->getInput() === "stein") ? "highlight" : '') : ''; ?>" value="">
        </form>
        <form method="post">
            <input type="hidden" name="value" value="schere">
            <input type="submit" class="schere <?php echo($result) ? (($result->getInput() === "schere") ? "highlight" : '') : ''; ?>" value="">
        </form>
        <form method="post">
            <input type="hidden" name="value" value="papier">
            <input type="submit" class="papier <?php echo($result) ? (($result->getInput() === "papier") ? "highlight" : '') : ''; ?>" value="">
        </form>
    </div>
</body>
</html>