STEIN SCHERE PAPIER

Die Aufgabe, wie ich sie verstanden und sie mir übermittelt wurde lautete:

- Einfaches Stein, Schere, Papier Spiel implementieren
- unter Verwendung einer der bei euch eingesetzten Sprachen
- Zeitaufwand ca. eine Stunde

Ich habe mich für plain PHP entschieden, da ich etwas weniger Erfahrung in PHP habe. Für eine realistische Evaluierung zeige ich euch lieber meinen 'floor'.

Da ihr eine Browserbasierte Platform anbietet habe ich mich für eine GUI basierte Browserversion über z.B. ein CLI Version entschieden (hierfür würde ich ohnehin eher .py oder .sh verwenden)

Mir wären SVGs als Assets lieber gewesen aber ich konnte innerhalb der Zeitangabe keine Passenden finden.

Edit: Mir ist bereits die erste unsauberkeit in meinem Code aufgefallen. Die Class Attributes sollten im constructor befüllt werden.